import { Story } from "./story.model";

export interface Game {
  id: Number;
  user_id: Number;
  name: String;
  story: Story;
  description: String;
  created_at: String;
  updated_at: String;
}
