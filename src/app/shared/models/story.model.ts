import { Game } from "./game.model";

export interface Story {
  id: Number;
  user_id: Number;
  name: String;
  description: String;
  created_at: String;
  updated_at: String;
}
