import { Component, OnInit, Renderer, Renderer2 } from '@angular/core';
import { Story, StoryService } from "../story.service";
import { GameService } from "../game.service";
import { Game } from "../shared/models/game.model";
import { Router } from "@angular/router"

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {

  stories: Story[];
  errorMessage: string;

  constructor(private storyService: StoryService,
    private gameService: GameService,
    private router: Router,
    private renderer: Renderer2) { }

  ngOnInit() {
    this.getStories();
    this.renderer.addClass(document.body, 'page-light');
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'page-light');
  }
  getStories() {
    this.storyService.getStories()
      .subscribe((res: any[]) => {
        this.stories = res["data"];
      });
  }

  getStory() {
    this.storyService.getStories()
      .subscribe((res: any[]) => {
        this.stories = res["data"];
      });
  }

  newGame(storyId) {
    this.gameService.startGame(storyId)
      .subscribe(res => {
        this.router.navigate(['/game', res["data"]["id"]])
      });
  }
}