import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { StoryComponent } from "./story/story.component";
import { GameComponent } from "./game/game.component";

const routes: Routes = [
  { path: '', component: StoryComponent, data: { animation: 'StoryList' } },
  { path: 'story', component: StoryComponent, data: { animation: 'StoryList' } },
  { path: 'story/:id', component: StoryComponent, pathMatch: 'full', data: { animation: 'Story' } },
  { path: 'game/:id', component: GameComponent, pathMatch: 'full', data: { animation: 'Game' } },
  { path: 'game/:id/node/:node_id', component: GameComponent, pathMatch: 'full', data: { animation: 'Game' } },
  { path: 'game/:id/decision/:decision_id', component: GameComponent, pathMatch: 'full', data: { animation: 'Game' } },
  { path: '**', redirectTo: '', pathMatch: 'full' },];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    //onSameUrlNavigation: 'reload',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
