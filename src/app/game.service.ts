import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Story } from "./shared/models/story.model";
import { Game } from "./shared/models/game.model";

const API_URL: string = "http://localhost:8000/api";

@Injectable({
  providedIn: "root"
})
export class GameService {
  constructor(private http: HttpClient) {}

  getGame(gameId): Observable<Object[]> {
    return this.http.get<Object[]>(API_URL + "/game/" + gameId);
  }

  startGame(storyId, userId = 1): Observable<Object[]> {
    return this.http.post<Game[]>(API_URL + "/game/", {
      story_id: storyId,
      user_id: userId
    });
  }

  makeDecision(gameId, decisionsId): Observable<Object[]> {
    return this.http.post<Game[]>(
      API_URL + "/game/" + gameId + "/decision/" + decisionsId,
      {}
    );
  }
}
